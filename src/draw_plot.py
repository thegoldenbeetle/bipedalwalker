import matplotlib.pyplot as plt

def draw_plot(y_values, title):
    x_values = [i for i in range(1, len(y_values) + 1)]
    plt.plot(x_values, y_values)
    plt.title(title)
    plt.xlabel('episode num')
    plt.ylabel('reward')
    plt.grid(True)
    plt.show()