class RewardLogger:
    def __init__(self, log_filename, log_interval):
        self.log_file = open(log_filename,"w")
        self.log_interval = log_interval
        self.episode_reward = 0
        self.average_reward = 0
        self.max_average_reward = -300
        self.episodes_rewards = []
    
    # call after episode
    def log(self, episode_num):
        self.log_file.write('{}, {}\n'.format(episode_num, self.episode_reward))
        self.episodes_rewards.append(self.episode_reward)
        self.episode_reward = 0
        
        isMaxAverageRewardUpdated = False
        if episode_num % self.log_interval == 0:
            self.average_reward = self.average_reward / self.log_interval
            if self.average_reward > self.max_average_reward:
                self.max_average_reward = self.average_reward
                isMaxAverageRewardUpdated = True
            print("Episode: {}, Average Reward: {}".format(episode_num, self.average_reward))
            self.average_reward = 0
        return isMaxAverageRewardUpdated
    
    # call in episode, after getting reward from environment
    def append_reward(self, reward):
        self.average_reward += reward
        self.episode_reward += reward
    
    def get_rewards(self):
        return self.episodes_rewards
        
    def __del__(self):
        self.log_file.close()