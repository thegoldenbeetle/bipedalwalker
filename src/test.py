import gym
import torch
import numpy as np
from tqdm.notebook import tqdm
from PIL import Image

from TD3 import TD3
from reward_logger import RewardLogger

def test(model, environment, max_timesteps, launch_number, logs_directory, render=False):       
   
    rewardLogger = RewardLogger(logs_directory + "/log_test.txt", 10)
    
    for episode in tqdm(range(1, launch_number + 1)):
        episode_reward = 0
        state = environment.reset()
        
        for timestep in range(max_timesteps):
            action = model.get_action(state)
            state, reward, done, _ = environment.step(action)
            rewardLogger.append_reward(reward)
            
            if render:
                environment.render()

            if done:
                break
        
        rewardLogger.log(episode)

        environment.close()
    
    return rewardLogger.get_rewards()