import gym
import torch
import numpy as np
from tqdm.notebook import tqdm

from replay_buffer import ReplayBuffer
from reward_logger import RewardLogger

def train(environment, model,
          gamma, batch_size, exploration_noise,
          polyak, policy_noise, noise_clip, policy_delay,
          episodes, max_timesteps,
          logs_directory, save_directory, filename):
   
    replay_buffer = ReplayBuffer()
    
    rewardLogger = RewardLogger(logs_directory + "/log_train.txt", 10)
    
    for episode in tqdm(range(1, episodes + 1)):
        state = environment.reset()
        for timestep in range(max_timesteps):
            
            # select action and add exploration noise:
            action = model.get_action(state)
            action = action + np.random.normal(0, exploration_noise, size=environment.action_space.shape[0])
            action = action.clip(environment.action_space.low, environment.action_space.high)
            
            # transfer action to environment:
            next_state, reward, done, _ = environment.step(action)
            
            replay_buffer.add((state, action, reward, next_state, float(done)))
            state = next_state
            rewardLogger.append_reward(reward)
                        
            # if episode is done then update model:
            if done or timestep == (max_timesteps - 1):
                model.update(replay_buffer, timestep, batch_size, gamma, polyak, policy_noise, noise_clip, policy_delay)
                break
        
        # save model with max average reward
        isMaxAverageRewardUpdated = rewardLogger.log(episode)
        if isMaxAverageRewardUpdated:
            model.save(save_directory, filename)

    return rewardLogger.get_rewards()
