# BipedalWalker

BipedalWalker - is one of the environments in OpenAIGym. <br>
The purpose of the task is to teach the robot from this environment to walk. <br>
https://gym.openai.com/envs/BipedalWalker-v2/

The TD3 model was used for training. <br>
https://arxiv.org/pdf/1802.09477.pdf <br>
It is one of the best approaches for this task according to the paper above.

## Repository structure

In `requirements` dir in file `pip.txt` you can find necessary python packages for code running. Also you should have Python (version >= 3.5), pip3 and jupyter notebook.

In the `logs` dir you can find files `log_train.txt` and `log_test.txt` with results of teaching and testing model. They contain a table consisting of the episode number and the received reward.

In the `images` dir you can find files `train_plot.png`, `test_plot.png` and `test.gif` with one of the test launch of the model.

In the `models` dir you can find received model files. You can use them for testing or further training.

In the `src` dir you can find source files. They contain classes which describe the model architecture, methods for training and testing.

In the `bipedal_walker.ipynb` file you can run code for training, testing and see received results.

## Results

Max reward on train: 310.72

Number of test launches: 100 <br>
Mean reward on test: 297.7 <br>
Max reward on test: 299.4 <br>

Training and testing plots:

<table><tr><td><img src='images/train_plot.png'></td><td><img src='images/test_plot.png'></td></tr></table>

Launch:

![Launch](images/test.gif "Launch")
